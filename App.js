import React, {Component} from 'react';
import {View, Text} from 'react-native';
import MainMenu from './src/components/screens/MainMenu';
import Login from './src/components/screens/Login';
import Welcome from './src/components/screens/Welcome';
import QRScanner from './src/components/screens/QRScanner';
import Settings from './src/components/screens/Settings'
import AuthLoading from './src/components/screens/AuthLoading';
//import {createStackNavigator, createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator, createAppContainer, createSwitchNavigator} from '@react-navigation/stack';
import {MaterialIcons} from '@expo/vector-icons';
import {COLORS} from './src/constants/styles';
import {Button} from 'react-native-elements';
import {useScreens} from 'react-native-screens';
import Register from "./src/components/screens/Register";

useScreens();

class HomeScreen extends Component {
    static navigationOptions = ({navigation}) => {
        return {
            headerTitle: 'Aparcap',
            headerRight: (
                <Button
                    onPress={() => navigation.navigate('Settings')}
                    icon={
                        <MaterialIcons name="settings" size={30}/>
                    }
                    buttonStyle={{backgroundColor: COLORS.WHITE.CREAM_WHITE}}
                />
            ),
        };
    };


    render() {
        return (
            <View style={{flex: 1, backgroundColor: COLORS.WHITE.CREAM_WHITE}}>
                {<MainMenu navigation={this.props.navigation}/>}
            </View>
        );
    }
}

class LoginScreen extends Component {
    static navigationOptions = {
        header: null,
    };

    render() {
        return (
            <View style={{flex: 1, backgroundColor: COLORS.WHITE.CREAM_WHITE}}>
                {<Login navigation={this.props.navigation}/>}
            </View>
        );
    }
}

class RegisterScreen extends Component {
    static navigationOptions = {
        header: null,
    };

    render() {
        return (
            <View style={{flex: 1, backgroundColor: COLORS.WHITE.CREAM_WHITE}}>
                {<Register navigation={this.props.navigation}/>}
            </View>
        );
    }
}

class WelcomeScreen extends Component {
    static navigationOptions = {
        header: null,
    };

    render() {
        return (
            <View style={{flex: 1, backgroundColor: COLORS.WHITE.CREAM_WHITE}}>
                <Welcome navigation={this.props.navigation}/>
            </View>
        );
    }
}

class SettingsScreen extends Component {
    render() {
        return (
            <Settings navigation={this.props.navigation}/>
        );
    }
}

class QRScannerScreen extends Component {
    static navigationOptions = {
        headerTransparent: true,
        headerTitle: 'Aparcap',
        headerLeft: null
    };

    render() {
        return (

            <QRScanner/>

        );
    }
}

class AuthLoadingScreen extends Component {
    static navigationOptions = {
        header: null,
    };

    render() {
        return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <AuthLoading navigation={this.props.navigation}/>
            </View>
        );
    }
}

const AppStack = createStackNavigator({Home: HomeScreen, Scanner: QRScannerScreen, Settings: SettingsScreen});
const AuthStack = createStackNavigator({
    SignIn: LoginScreen,
    SignUp: RegisterScreen,
    Welcome: WelcomeScreen
}, {initialRouteName: 'Welcome'});

const AppContainer = createAppContainer(createSwitchNavigator(
    {
        AuthLoading: AuthLoadingScreen,
        App: AppStack,
        Auth: AuthStack,
    },
    {
        initialRouteName: 'AuthLoading',
    }
));

export default class App extends Component {
    render() {
        return <AppContainer/>;
    }
}

