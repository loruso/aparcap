Prototype (play on the top-right): https://www.figma.com/file/0qmAXVoBHDIUNBaucdx2X3tM/Full-App?node-id=0%3A1

Expo setup: https://expo.io/learn

**DOCS:**
* Expo: https://docs.expo.io/versions/latest/
* React Native: https://facebook.github.io/react-native/docs/getting-started.html
* React Navigation: https://reactnavigation.org/docs/en/getting-started.html
* React Native Elements: https://react-native-training.github.io/react-native-elements/docs/getting_started.html
* Styled Components: https://www.styled-components.com/docs/basics
<p float="left">
    <img src="https://i.imgur.com/3EqwspK.png"  width="360" height="640">
    <img src="https://i.imgur.com/Sv2UaK4.png"  width="360" height="640">
    <img src="https://i.imgur.com/qJ8D09o.png"  width="360" height="640">
    <img src="https://i.imgur.com/iTLczo1.png"  width="360" height="640">
    <img src="https://i.imgur.com/XVQDQ0d.png"  width="360" height="640">
    <img src="https://i.imgur.com/yA0G3E8.png"  width="360" height="640">
</p>