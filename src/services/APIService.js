import { AsyncStorage } from 'react-native';
// const rootURL = 'https://laboapi.herokuapp.com/api/rest-auth/';
const rootURL = 'http://192.168.8.231:8080/';
const loginURL = rootURL+'authenticate';
const registrationURL = rootURL+'register';


let token;

export function hasToken(){
  return token != null;
  // return true;
}
function getToken(){
  return token;
}
function setToken(_token){
  token = _token;
}
function flushToken() {
  token = null;
}
export function login(username,password) {
  fetch(loginURL, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify({
      username: username,
      password: password,
    }),
  }).then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson);
      return true;
    })
    .catch((error) => {
      console.error(error);
      return false;
    });
}

export function register(email,password,licensePlate,firstName,lastName) {
  console.log("********" + password);
  fetch(registrationURL, {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      username: email,
      password: password,
      licensePlate: licensePlate,
      firstName: firstName,
      lastName: lastName,
    }),
  }).then((response) => response.json())
    .then((responseJson) => {
      console.log(responseJson);
      setToken(responseJson.token);
      return true;
    })
    .catch((error) => {
      console.error(error);
      return false;
    });
}