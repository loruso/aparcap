import React, {Component} from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';

import {COLORS} from '../../constants/styles';
import {Button} from 'react-native-elements';
import {MaterialCommunityIcons, MaterialIcons} from '@expo/vector-icons';
import { AppLoading } from 'expo';
import * as Font from 'expo-font';
import {hasToken} from '../../services/APIService'

function cacheFonts(fonts) {
    return fonts.map(font => Font.loadAsync(font));
}

export default class AuthLoading extends Component {

    constructor(props) {
        super(props);
        this._bootstrapAsync();

    }

    state = {
        isReady: false,
    };

    _bootstrapAsync = async () => {
        // This will switch to the App screen or Auth screen and this loading
        // screen will be unmounted and thrown away.
        this.props.navigation.navigate(hasToken() ? 'App' : 'Auth');
    };

    async _loadAssetsAsync() {
        const fontAssets = cacheFonts([{'libre-franklin-regular': require('../../../assets/fonts/LibreFranklin-Regular.ttf')}, MaterialCommunityIcons.font, MaterialIcons.font]);

        await Promise.all([...fontAssets]);
    }

    render() {
        if (!this.state.isReady) {
            return (
                <AppLoading
                    startAsync={this._loadAssetsAsync}
                    onFinish={() => this.setState({isReady: true})}
                    onError={console.warn}
                />
            );
        }
        return (
            <View style={styles.container}>
                <View style={[styles.box, styles.whitebox, {justifyContent: 'center', flex: 1.5}]}>
                    <Text style={styles.header}> <Image style={styles.logo}
                                                        source={require("../../../assets/splash.png")}
                                                        resizeMode='contain'/> Aparcap</Text>
                </View>

                <View style={[styles.box, styles.redbox, {justifyContent: 'flex-start', flex: 2}]}>
                    <View style={styles.hiddenRectangle}/>
                    <View style={styles.diagonalRectangle}/>
                </View>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    logo: {
        width: 50,
        height: 50
    },
    header: {
        fontFamily: 'libre-franklin-regular',
        color: COLORS.BLUE.DARK_BLUE,
        fontSize: 45,
        alignSelf: 'center'
    },
    container: {
        flex: 1,
    },
    box: {
        alignItems: 'center',
    },
    diagonalRectangle: {
        width: 475,
        height: 100,
        alignSelf: 'center',
        backgroundColor: COLORS.BLUE.LIGHT_BLUE,
        transform: [
            {rotate: '155deg'}
        ]
    },
    hiddenRectangle: {
        width: 225,
        height: 80,
        alignSelf: 'flex-start',
        backgroundColor: COLORS.WHITE.CREAM_WHITE,
        position: 'absolute',
    },
    redbox: {
        backgroundColor: COLORS.RED.LIGHT_RED,
    },
    whitebox: {
        backgroundColor: COLORS.WHITE.CREAM_WHITE,
    },
    btn: {
        height: 55,
        width: 300,
        justifyContent: 'center',
        alignSelf: 'flex-end',
        borderRadius: 5,
    },
    redbtn: {
        borderColor: COLORS.WHITE.CREAM_WHITE,
        borderWidth: 2,
        marginTop: 20,
        backgroundColor: COLORS.RED.LIGHT_RED
    },
    bluebtn: {
        backgroundColor: COLORS.BLUE.LIGHT_BLUE,
    },
    btnText: {
        fontFamily: 'libre-franklin-regular',
        fontSize: 20,
        color: COLORS.WHITE.CREAM_WHITE,
    }
});