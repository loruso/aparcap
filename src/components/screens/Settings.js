import React, {Component} from 'react';
import {View, StyleSheet, Picker} from 'react-native';
import {COLORS} from '../../constants/styles';
import {Button} from 'react-native-elements';
import {MaterialCommunityIcons, MaterialIcons} from '@expo/vector-icons';
import { AppLoading } from 'expo';

import * as Font from 'expo-font';

function cacheFonts(fonts) {
  return fonts.map(font => Font.loadAsync(font));
}

export default class Settings extends Component {
  state = {
    isReady: false,
  };

  async _loadAssetsAsync() {
    const fontAssets = cacheFonts([{'libre-franklin-regular': require('../../../assets/fonts/LibreFranklin-Regular.ttf')}, MaterialCommunityIcons.font, MaterialIcons.font]);

    await Promise.all([...fontAssets]);
  }

  render() {
    if (!this.state.isReady) {
      return (
        <AppLoading
          startAsync={this._loadAssetsAsync}
          onFinish={() => this.setState({isReady: true})}
          onError={console.warn}
        />
      );
    }
    return (
      <View style={[styles.container, styles.yellowbg]}>
        <View style={styles.buttonsContainer}>

          <Button
            title="OPTION 1"
            titleStyle={styles.btnText}
            buttonStyle={[styles.btn, styles.whitebtn]}
            onPress={() =>
              this.props.navigation.navigate('Home')}
          />
            <View style={styles.Rectangle}/>
          <Button
            title="OPTION 2"
            titleStyle={styles.btnText}
            buttonStyle={[styles.btn, styles.whitebtn]}
            onPress={() =>
              this.props.navigation.navigate('Home')}
          />
          <Button
            title="ABOUT US"
            titleStyle={styles.btnText}
            buttonStyle={[styles.btn, styles.whitebtn]}
            onPress={() =>
              this.props.navigation.navigate('Home')}
          />
          <View style={[styles.Rectangle,{marginTop:200}]}/>
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  buttonsContainer: {
    width: '100%',
    marginTop: 30,
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  yellowbg: {
    backgroundColor: COLORS.YELLOW.LIGHT_YELLOW,
  },
  btn: {
    height: 55,
    marginTop: 5,
  },
  whitebtn: {
    backgroundColor: COLORS.WHITE.CREAM_WHITE,
  },
  btnText: {
    fontFamily: 'libre-franklin-regular',
    fontSize: 20,
    color: COLORS.BLUE.DARK_BLUE,
  },
  Rectangle: {
    width: 80,
    height: 240,
    alignSelf: 'flex-start',
    backgroundColor: COLORS.BLUE.DARK_BLUE,
    position: 'absolute',
  },
});