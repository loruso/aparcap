import React, {Component} from 'react';
import {View, StyleSheet, Text, ActivityIndicator} from 'react-native';
import {COLORS} from '../../constants/styles';
import {Button} from 'react-native-elements';
import {MaterialCommunityIcons, MaterialIcons} from '@expo/vector-icons';
import { AppLoading } from 'expo';

import { BarCodeScanner } from 'expo-barcode-scanner';
import * as Permissions from 'expo-permissions';
import Constants from 'expo-constants';
import * as Font from 'expo-font';

export default class QRScanner extends Component {
    state = {
        hasCameraPermission: null,
        scanned: false,
    };

    async componentDidMount() {
        const {status} = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({hasCameraPermission: status === 'granted'});
    }

    render() {
        const {hasCameraPermission, scanned} = this.state;

        if (hasCameraPermission === null) {
            return <View styles={styles.container}><ActivityIndicator size="large" style={styles.spinningIcon}/></View>;
        }
        if (hasCameraPermission === false) {
            return <Text>No access to camera</Text>;
        }
        return (
            <View
                style={{
                    flex: 1,
                    justifyContent: 'flex-end',
                }}>
                <BarCodeScanner
                    onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
                    style={StyleSheet.absoluteFillObject}
                />

                {scanned && (
                    <Button
                        title={'Tap to Scan Again'}
                        onPress={() => this.setState({scanned: false})}
                    />
                )}
            </View>
        );
    }

    handleBarCodeScanned = ({type, data}) => {
        this.setState({scanned: true});
        alert(`Bar code with type ${type} and data ${data} has been scanned!`);
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center'
    },
    spinningIcon: {
        color: COLORS.BLUE.DARK_BLUE
    }
});