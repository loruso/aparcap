import React, {Component} from 'react';
import {View, StyleSheet, Picker} from 'react-native';
import {COLORS} from '../../constants/styles';
import {Button} from 'react-native-elements';
import {MaterialCommunityIcons, MaterialIcons} from '@expo/vector-icons';
import { AppLoading } from 'expo';

import * as Font from 'expo-font';

function cacheFonts(fonts) {
    return fonts.map(font => Font.loadAsync(font));
}

export default class MainMenu extends Component {
    state = {
        isReady: false,
    };

    async _loadAssetsAsync() {
        const fontAssets = cacheFonts([{'libre-franklin-regular': require('../../../assets/fonts/LibreFranklin-Regular.ttf')}, MaterialCommunityIcons.font, MaterialIcons.font]);

        await Promise.all([...fontAssets]);
    }

    render() {
        if (!this.state.isReady) {
            return (
                <AppLoading
                    startAsync={this._loadAssetsAsync}
                    onFinish={() => this.setState({isReady: true})}
                    onError={console.warn}
                />
            );
        }
        return (
            <View style={styles.container}>
                <View style={[styles.box, styles.bluebox]}>
                    <View style={styles.pickerRectangle}>
                        <View style={[styles.invertedTriangle, styles.whiteTriangle]}/>
                        <Picker
                            selectedValue={this.state.language}
                            style={styles.picker}

                            onValueChange={(itemValue, itemIndex) =>
                                this.setState({language: itemValue})
                            }>
                            <Picker.Item label="Patente1" value="p1"/>
                            <Picker.Item label="Patente2" value="p2"/>
                        </Picker>
                    </View>
                    <View style={[styles.triangle, styles.redTriangle]}/>
                </View>
                <View style={[styles.box, styles.redbox]}>
                    <Button
                        icon={
                            <MaterialCommunityIcons style={styles.btnIcon} name="qrcode" size={30}/>
                        }
                        iconRight
                        title="SCAN CODE"
                        raised
                        titleStyle={styles.btnText}
                        buttonStyle={[styles.btn, styles.bluebtn]}
                        containerStyle={{borderRadius: 5}}
                        onPress={() =>
                            this.props.navigation.navigate('Scanner')}
                    />
                    <View style={[styles.triangle, styles.blueTriangle]}/>
                </View>
                <View style={[styles.box, styles.bluebox]}>
                    <Button
                        icon={
                            <MaterialIcons style={styles.btnIcon} name="attach-money" size={30}/>
                        }
                        iconRight
                        raised
                        title="PAYMENTS"
                        titleStyle={styles.btnText}
                        buttonStyle={[styles.btn, styles.redbtn]}
                        containerStyle={{borderRadius: 5}}
                    />
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    box: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    bluebox: {
        backgroundColor: COLORS.BLUE.LIGHT_BLUE,
    },
    redbox: {
        backgroundColor: COLORS.RED.LIGHT_RED,
    },
    btn: {
        height: 55,
        width: 300,
        justifyContent: 'space-between',
        borderRadius: 5,
    },
    redbtn: {
        backgroundColor: COLORS.RED.LIGHT_RED,
    },
    bluebtn: {
        backgroundColor: COLORS.BLUE.LIGHT_BLUE,
    },
    btnText: {
        fontFamily: 'libre-franklin-regular',
        fontSize: 20,
        color: COLORS.WHITE.CREAM_WHITE,
    },
    btnIcon: {
        color: COLORS.WHITE.CREAM_WHITE,
    },
    pickerRectangle: {
        width: 302,
        height: 57,
        backgroundColor: 'transparent',
        borderRadius: 5,
        borderWidth: 2,
        borderColor: COLORS.WHITE.CREAM_WHITE,
    },
    picker: {
        height: 55,
        width: 300,
        color: COLORS.WHITE.CREAM_WHITE,
        backgroundColor: 'transparent',
        //lo de abajo para tamanio de la fuente de texto
        // transform: [
        //     { scaleX: 1.2 },
        //     { scaleY: 1.2 },
        // ]
    },
    triangle: {
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: 12.5,
        borderRightWidth: 12.5,
        borderBottomWidth: 25,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        alignSelf: 'flex-end',
        position: 'absolute'
    },
    invertedTriangle: {
        width: 0,
        height: 0,
        backgroundColor: 'transparent',
        borderStyle: 'solid',
        borderLeftWidth: 7.5,
        borderRightWidth: 7.5,
        borderBottomWidth: 15,
        borderLeftColor: 'transparent',
        borderRightColor: 'transparent',
        position: 'absolute',
        marginTop: 20,
        marginLeft: 260,
        transform: [
            {rotate: '180deg'}
        ]
    },
    blueTriangle: {
        borderBottomColor: COLORS.BLUE.LIGHT_BLUE,
    },
    redTriangle: {
        borderBottomColor: COLORS.RED.LIGHT_RED,
    },
    whiteTriangle: {
        borderBottomColor: COLORS.WHITE.CREAM_WHITE,
    }
});