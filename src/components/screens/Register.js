import React, {Component} from 'react'
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Keyboard
} from 'react-native'
import {KeyboardAvoidingView, ImageBackground} from 'react-native';
import {COLORS} from "../../constants/styles";
import {Button, Divider} from "react-native-elements";
import {login, register} from "../../services/APIService"
import Dialog, {DialogContent, DialogTitle, SlideAnimation} from "react-native-popup-dialog";


export default class Register extends Component {
    state = {
        email: '',
        password: '',
        patente: '',
        nombre: '',
        apellido: '',
        passwordRepeat: '',
        popup: false
    };

    onRegister() {
        if (register( this.state.email, this.state.password, this.state.patente, this.state.nombre, this.state.apellido)===false || this.state.password !== this.state.passwordRepeat) {
            this.setState({
                popup: true,
            });
        } else {
            console.log("Registro exitoso!");
            this.props.navigation.navigate('Login')
        }
    }

    render() {

        return (
            //Donot dismis Keyboard when click outside of TextInput
            <ImageBackground source={require('../../../assets/parkinglot.png')} style={{width: '100%', height: '100%'}}>
                <KeyboardAvoidingView style={[styles.container, styles.up]} behavior="padding" enabled>
                    <Divider style={styles.divider}>
                        <Dialog
                            onDismiss={() => {
                                this.setState({popup: false});
                            }}
                            onTouchOutside={() => {
                                this.setState({popup: false});
                            }}
                            visible={this.state.popup}
                            dialogTitle={<DialogTitle title="Register Error"/>}
                            dialogAnimation={new SlideAnimation({slideFrom: 'bottom'})}>
                            <DialogContent>
                                <Text>Values must contain only letters, numbers, and @/./+/-/_ characters.</Text>
                            </DialogContent>
                        </Dialog>
                    </Divider>
                    <View style={styles.textInputContainer}>
                        <TextInput
                            value={this.state.email}
                            keyboardType='email-address'
                            textContentType='emailAddress'
                            onChangeText={(email) => this.setState({email})}
                            placeholder="Enter your email"
                            style={styles.textInput}
                        />
                    </View>

                    <View style={styles.textInputContainer}>
                        <TextInput
                            value={this.state.password}
                            onChangeText={(password) => this.setState({password})}
                            placeholder={'Enter your password'}
                            secureTextEntry={true}
                            style={styles.textInput}
                        />
                    </View>
                    <View style={styles.textInputContainer}>
                        <TextInput
                          value={this.state.passwordRepeat}
                          onChangeText={(passwordRepeat) => this.setState({passwordRepeat})}
                          placeholder={'Repeat your password'}
                          secureTextEntry={true}
                          style={styles.textInput}
                        />
                    </View>
                    <View style={styles.textInputContainer}>
                        <TextInput
                            value={this.state.patente}
                            onChangeText={(patente) => this.setState({patente})}
                            placeholder={'Enter your license plate'}
                            style={styles.textInput}
                        />
                    </View>
                    <View style={styles.textInputContainer}>
                        <TextInput
                          value={this.state.nombre}
                          onChangeText={(nombre) => this.setState({nombre})}
                          placeholder={'Enter your nombre'}
                          style={styles.textInput}
                        />
                    </View>
                    <View style={styles.textInputContainer}>
                        <TextInput
                          value={this.state.apellido}
                          onChangeText={(apellido) => this.setState({apellido})}
                          placeholder={'Enter your last name'}
                          style={styles.textInput}
                        />
                    </View>
                    <View style={styles.down}>

                        <Button
                            onPress={this.onRegister.bind(this)}
                            type="clear"
                            title="REGISTER"
                            titleStyle={styles.btnText}
                            buttonStyle={[styles.btn, styles.redbtn, {justifyContent: 'center'}]}
                        />

                    </View>

                </KeyboardAvoidingView>
            </ImageBackground>


        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch',
        backgroundColor: COLORS.RED.TRANSPARENT_RED
    },
    up: {

        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    down: {

        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    title: {
        color: COLORS.WHITE.CREAM_WHITE,
        textAlign: 'center',
        width: 400,
        fontSize: 23
    },
    textInputContainer: {
        paddingHorizontal: 10,
        borderRadius: 6,
        marginBottom: 20,
        backgroundColor: COLORS.WHITE.CREAM_WHITE
    },
    textInput: {
        width: 280,
        height: 45
    },
    btn: {
        height: 55,
        width: 300,
        justifyContent: 'space-between',
        borderColor: COLORS.WHITE.CREAM_WHITE,
        borderWidth: 1,
        borderRadius: 7
    },
    redbtn: {
        backgroundColor: COLORS.RED.TRANSPARENT_RED
    },

    btnText: {
        fontSize: 20,
        color: COLORS.WHITE.CREAM_WHITE,
    },
    loginButtonTitle: {
        fontSize: 18,
        color: 'white'
    },
    facebookButton: {
        width: 30,
        height: 45,
        borderRadius: 6,
        justifyContent: 'center',
    },
    line: {
        height: 1,
        flex: 2,
        backgroundColor: 'black'
    },
    textOR: {
        flex: 1,
        textAlign: 'center'
    },
    divider: {
        flexDirection: 'row',
        height: 40,
        width: 298,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLORS.TRANSPARENT
    }
});
