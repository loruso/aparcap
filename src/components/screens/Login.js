import React, {Component} from 'react'
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    TouchableOpacity,
    TouchableWithoutFeedback,
    Keyboard
} from 'react-native'
import {KeyboardAvoidingView, ImageBackground} from 'react-native';
import {COLORS} from "../../constants/styles";
import {Button, Divider} from "react-native-elements";
import Dialog, {
    DialogTitle,
    DialogContent,
    SlideAnimation
} from 'react-native-popup-dialog';
import {login} from "../../services/APIService"
import {Google} from 'expo'


export default class Login extends Component {
    state = {
        email: '',
        password: '',
        popup: false
    };

    onLogin() {
        if (login(this.state.email, this.state.password)===false) {
            this.setState({
                popup: true,
            });
            console.log("Cartel de error");
        } else {
            console.log("Autenticacion exitosa");
            this.props.navigation.navigate('Home')
        }
    }

    render() {

        return (
            //Do not dismiss Keyboard when click outside of TextInput


            <ImageBackground source={require('../../../assets/parkinglot.png')} style={{width: '100%', height: '100%'}}>
                <Divider style={styles.divider}>
                    <Dialog
                        onDismiss={() => {
                            this.setState({popup: false});
                        }}
                        onTouchOutside={() => {
                            this.setState({popup: false});
                        }}
                        visible={this.state.popup}
                        dialogTitle={<DialogTitle title="Login Error"/>}
                        dialogAnimation={new SlideAnimation({slideFrom: 'bottom'})}>
                        <DialogContent>
                            <Text>User or passwords are invalid</Text>
                        </DialogContent>
                    </Dialog>
                </Divider>
                <KeyboardAvoidingView style={[styles.container, styles.up]} behavior="padding" enabled>
                    <Divider style={styles.divider}>
                    </Divider>
                    <View style={styles.center}>
                        <View style={styles.textInputContainer}>
                            <TextInput
                                value={this.state.email}
                                keyboardType='email-address'
                                textContentType='emailAddress'
                                onChangeText={(email) => this.setState({email})}
                                placeholder="Enter your email"
                                style={styles.textInput}
                            />


                        </View>


                        <View style={styles.textInputContainer}>
                            <TextInput
                                value={this.state.password}
                                onChangeText={(password) => this.setState({password})}
                                placeholder={'Enter your password'}
                                secureTextEntry={true}
                                style={styles.textInput}
                            />
                        </View>
                    </View>
                    <View style={styles.down}>

                        <Button
                            onPress={this.onLogin.bind(this)}
                            type="clear"
                            title="Login"
                            titleStyle={styles.btnText}
                            buttonStyle={[styles.btn, styles.bluebtn, {justifyContent: 'center'}]}
                        />

                    </View>

                </KeyboardAvoidingView>
            </ImageBackground>


        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'stretch',
        backgroundColor: COLORS.BLUE.TRANSPARENT_LIGHT_BLUE,
    }, customBackgroundDialog: {
        opacity: 0.5,
        backgroundColor: '#000',
    },
    middle: {

        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    up: {

        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    down: {

        flexDirection: 'column',
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    title: {
        color: COLORS.WHITE.CREAM_WHITE,
        textAlign: 'center',
        width: 400,
        fontSize: 23
    },
    textInputContainer: {
        paddingHorizontal: 10,
        borderRadius: 6,
        marginBottom: 20,
        backgroundColor: COLORS.WHITE.CREAM_WHITE//a = alpha = opacity
    },
    textInput: {
        width: 280,
        height: 45
    },
    btn: {
        height: 55,
        width: 300,
        justifyContent: 'space-between',
        borderColor: COLORS.WHITE.CREAM_WHITE,
        borderWidth: 1,
        borderRadius: 7
    }, bluebtn: {
        backgroundColor: COLORS.BLUE.LIGHT_BLUE,
    },

    btnText: {
        fontSize: 20,
        color: COLORS.WHITE.CREAM_WHITE,
    },
    loginButtonTitle: {
        fontSize: 18,
        color: 'white'
    },
    facebookButton: {
        width: 30,
        height: 45,
        borderRadius: 6,
        justifyContent: 'center',
    },
    line: {
        height: 1,
        flex: 2,
        backgroundColor: 'black'
    },
    textOR: {
        flex: 1,
        textAlign: 'center'
    },
    divider: {
        flexDirection: 'row',
        height: 40,
        width: 298,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLORS.TRANSPARENT
    }
})
