export const COLORS = {
    YELLOW: {
        LIGHT_YELLOW: '#FFE66D',
    },
    BLUE: {
        LIGHT_BLUE: '#4ECDC4',
        DARK_BLUE: '#1A535C',
        TRANSPARENT_LIGHT_BLUE: 'rgba(76, 205, 196, 0.5)'
    },
    RED: {
        LIGHT_RED: '#FF6B6B',
        TRANSPARENT_RED: 'rgba(255, 107, 107, 0.5)'
    },
    WHITE: {
        CREAM_WHITE: '#F7FFF7',
    },
    TRANSPARENT: 'rgba(255,255,255,0)',
};
